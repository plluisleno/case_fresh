<a class="col-lg-4 col-md-6 col-sm-6 col-xs-12" id="my-forest" href="{$my_forest_display}">
    <span class="link-item">
        <i class="material-icons">nature_people</i>
        {l s='My forest' d='weforest'}
    </span>
</a>