{extends file=$layout}
{block name='content'}
  <section id="main">
    {block name='page_content_container'}
        <div class="container-fluid">
            <div class="row" id="green-leaves">
                <div class="offset-md-8 col-4">
                    {l s='MY FOREST' mod='weforest'}
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 mt-1">
                    <p>{l s='Through this agreement with We Forest, Freshly Cosmetics is going to plant one tree for any order over 50€. This is a representation of all the trees that will be planted thanks to your orders.' mod='weforest'}</p>
                </div>
                <div class="col-md-6">
                    <img src="{$urls['base_url']}/modules/weforest/views/img/autumn-forest.jpeg" class="img-fluid"/>
                </div>
                <div class="col-md-6">
                    <div class="row">
                        {foreach from=$my_forest_info item=tree}
                            <div class="col-sm-4 tree">
                                <img src="{$urls['base_url']}/modules/weforest/views/img/icono-arbol.PNG" class="img-fluid"/>
                                <p>
                                    {l s='With your order on %1$s one tree was planted on Zambia' sprintf=[$tree.date_added] mod='weforest'}
                                </p>
                            </div>
                        {/foreach}
                    </div>
                    <h3>{l s='Freshly Challenge!' mod='Modules.Weforest'}</h3>
                    <p>{l s='This year we hope that we can reach up to 400 trees planted! Will you help us get there?' mod='weforest'}</p>
                    <div class="progress">
                        <div class="progress-bar" role="progressbar" style="width: {$total_trees/400*100}%;" aria-valuenow="{$total_trees}" aria-valuemin="0" aria-valuemax="4000"></div>  
                    </div>
                    <span style="margin-left: {$total_trees/400*100}%;">{$total_trees}</span>
                </div>
            </div>
        </div>
    {/block}
  </section>

{/block}


