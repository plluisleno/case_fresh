<?php
/**
 * 2007-2018 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2018 PrestaShop SA
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 */

class CustomerWeforest extends ObjectModel
{
    /** @var int Customer id */
    public $id_customer;

    /** @var string Object creation date */
    public $date_added;

    public static $definition = array(
        'table' => 'weforest',
        'primary' => 'id_weforest',
        'fields' => array(
            'id_customer' =>    array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true),
            'date_added' =>     array('type' => self::TYPE_DATE, 'validate' => 'isDate'),
        ),
    );
    
    /* Modify construct for passing customer id when instantiating object */
    public function __construct($id_customer, $id = null, $id_lang = null)
    {
        parent::__construct($id, $id_lang);

        $this->id_customer = $id_customer;
        $this->date_added = date('Y-m-d');
    }

    public static function getCustomerTrees($id_customer, $date)
    {   
        $customer_trees = Db::getInstance()->executeS(
            'SELECT `date_added`
            FROM `'._DB_PREFIX_.'weforest` 
            WHERE `id_customer`='.$id_customer.' AND `date_added` >= '.$date
        );

        return $customer_trees;
    }

    public static function getTotalTrees()
    {
        $total_trees = (int)Db::getInstance()->getValue('
            SELECT COUNT(*)
            FROM `'._DB_PREFIX_.'weforest`'
        );
        
        return $total_trees;
    }

}
